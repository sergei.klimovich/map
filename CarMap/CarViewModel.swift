//
//  CarViewModel.swift
//  CarMap
//
//  Created by user on 19.03.2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class CarViewModel: ObservableObject {
    @Published var carsArray : [CarModel] = []
    init() {
        getCar()
    }
    
    func getCar()  {
            let url = "http://cars.areas.su/cars"
            AF.request(url, method: .get).validate().responseJSON { [self]response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    for i in 0..<json.count{
                        carsArray.append(CarModel(id: json[i]["id"].stringValue, model: json[i]["model"].stringValue, lon: json[i]["lon"].intValue, lan: json[i]["lat"].intValue))
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
}
