//
//  CarModel.swift
//  CarMap
//
//  Created by user on 19.03.2021.
//

import Foundation

struct CarModel:Hashable {
    let id, model:String
    let lon, lan:Int
}
